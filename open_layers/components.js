class Shuttle extends React.Component{

    constructor(props){
        super(props);

        var usedParkings = [];
        props.parkings.map((parking, idx) => {
            // If parking has shuttle
            if (parking.shuttle!=0){
                usedParkings.push([parking, idx]);
            }
        });

        this.state = {
            // State.parkings = parking data + original index
            shuttles: props.shuttles,
            usedParkings: usedParkings
        };
    }

    del(parking) {
        console.log(parking);
        delete window.shuttles[parking["shuttle"]]; // Remove from shuttles
        parking["shuttle"] = "0";                   // Remove shuttle from parking
        this.forceUpdate();
    }

    calc(e, idx){
        //TODO bereken theoretisch
        console.log(jshelper.calcShuttle());
    }

    handleChange(e, shuttle) {
        var value = e.target.value;
        if(e.target.type=="number") value = parseFloat(e.target.value);

        shuttle[e.target.name] = value;
        this.forceUpdate();
    }


    onSelectChange(e, mapping){
        var oldPark = mapping[0];
        var origIndex = mapping[1];
        var newParkIndex = Number(e.target.value);
        var newPark = window.parkings[newParkIndex];

        if(e.target.value) {
            newPark.shuttle = oldPark.shuttle; // Set new
            oldPark.shuttle = "0";             // Reset previous
        }

        // Replace in copy for component
        var toBeRep = this.state.usedParkings.indexOf(mapping);
        this.state.usedParkings[toBeRep] = [newPark, newParkIndex];

        this.forceUpdate();
    }

    addRow() {
        // find first unused parking
        var pIndx = parkings.findIndex(parking => parking.shuttle==0);
        var p = parkings[pIndx];
        var newID = Math.max.apply(null, Object.keys(shuttles)) + 1;
        p.shuttle = newID;
        window.shuttles[newID] = {"cost": 0, "duration": 0};

        this.state.usedParkings.push([p, pIndx]);

        this.forceUpdate();
    }

    render(){
        console.log(this.state.usedParkings);

        return (
          <div>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Parking</th>
                  <th scope="col">Duration(s)</th>
                  <th scope="col">Cost(€)</th>
                  <th scope="col">Calculate</th>
                  <th scope="col">Remove</th>
                </tr>
              </thead>
              <tbody>
                 {
                   this.state.usedParkings.map((mapping, index) => {
                     var parking = mapping[0];
                     var origIndex = mapping[1];
                     var shuttle = shuttles[parking.shuttle];

                     return (
                       <tr key={parking.shuttle}>
                         <td>
                           <select value={origIndex}
                                   onChange={(e)=>{this.onSelectChange(e, mapping);}}>
                             {
                               window.parkings.map((p, idx) => {
                                  // If free parking or selected
                                  if(p && (!this.state.usedParkings.some((ma)=>ma[0]==p) || p==parking)){
                                    return (
                                      <option key={idx} value={idx}>
                                        {p.name}
                                      </option>
                                    )
                                  }
                               })
                             }
                           </select>
                         </td>
                         <td>
                            <input name="duration" value={shuttle.duration}
                                   onChange={(e)=>this.handleChange(e,shuttle)} type="number">
                            </input>
                         </td>
                         <td>
                           <input name="cost" value={shuttle.cost}
                                  onChange={(e)=>this.handleChange(e,shuttle)} type="number" step="0.01">
                            </input>
                         </td>
                         <td>
                           <button onClick={this.calc}>Calc</button>
                         </td>
                         <td>
                           <a onClick={() => {confirm('Are you sure?') ? this.del(parking) : null}}
                              className="btn btn-xs btn-danger">
                             <span className="glyphicon glyphicon-trash"></span>&nbsp;
                           </a>
                         </td>
                       </tr>
                     )
                   })
                 }
              </tbody>
            </table>
            <button className="btn btn-xs btn-success" onClick={() => {this.addRow();}}>
                <span className="glyphicon glyphicon-plus"></span>&nbsp;
                Add shuttle
            </button>
          </div>
        )
    }
};


class LayerList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            showFeatures: false
        }

        props.layers.map((layer) => {
            if(layer.get('title')) this.state[layer.get('title')] = false;

            //Add changelistener for addFeatures() layer source
            layer.getSource().on('addfeature', (() => {this.forceUpdate();}));
        });
    }

    onItemClick (layer, e) {
        layer.setVisible(e.target.checked);
    }

    toggle (layer) {
        this.state[layer] = !this.state[layer];
        this.forceUpdate();
    }

    render(){

        return (
          <div className="form-group-lg">
            <label className="control-label">Layers</label>
            <div className="list-group list-group-root well">
              {
                this.props.layers.map((layer) => {
                    if(layer.get('title')){
                        return (
                          <div key={layer.get('title')}>
                            <a className="list-group-item">
                                <span onClick={() => this.toggle(layer.get('title'))}
                                      className={`glyphicon ${this.state[layer.get('title')] ? "glyphicon-chevron-down" : "glyphicon-chevron-right"}`}/>
                                <input id={layer.get('title')} type="checkbox"
                                       defaultChecked={layer.getVisible()}
                                       onChange={(event) => this.onItemClick(layer, event)}/>
                                <label className="checkbox-inline">{layer.get('title')}</label>
                            </a>
                            {this.state[layer.get('title')] && <FeatureList style="padding-left: 30px;" layer={layer}/>}
                          </div>
                        )
                    }
                })
              }
            </div>
          </div>
        );
    }
}


class FeatureList extends React.Component {

    onItemClick (feature, e) {
        if(feature.getStyle()) {
            feature.setStyle();
        } else {
            feature.setStyle(new ol.style.Style({ visibility: 'hidden' }));
        }
    }

    render(){
        return (
            <div className="list-group">
                {
                    this.props.layer.getSource().getFeatures().map((feature) => {
                        if(feature.getProperties().name){
                            return (
                                <a key={feature.getProperties().name} className="list-group-item">
                                    <input id={feature.getProperties().name} type="checkbox"
                                           defaultChecked={true}
                                           onChange={(event) => this.onItemClick(feature, event)}/>
                                    <label className="checkbox-inline">{feature.getProperties().name}</label>
                                </a>
                            )
                        }
                    })
                }
            </div>
        );
    }
}


class FlowList extends React.Component {

    constructor(props) {
        super(props);

        //Add changelistener for addFeatures() layer source
        props.layer.getSource().on('addfeature', (() => {this.forceUpdate();}));
    }

    onItemClick (feature, e) {
        $(e.target).toggleClass('active');
        $(e.target).siblings().removeClass('active');

        console.log(window);

        window.hoverInteraction.getFeatures().clear(); //un highlight others
        window.hoverInteraction.getFeatures().push(feature); //highlight feature
    }

    render(){
        return (
            <ul className="list-group">
                {
                    this.props.layer.getSource().getFeatures().map((feature) => {
                        return (
                            <li key={feature.getId()}
                                className="flow-select list-group-item list-group-item-action"
                                onClick={(event) => this.onItemClick(feature, event)}>
                                {feature.getProperties().name}
                            </li>
                        )
                    })
                }
            </ul>
        );
    }
}

class TimeSlider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            playing: false,
            tick: 0
        };
    }

    componentDidMount() {
        //Executed after render
        this.slider = new Slider("#time", {
            step: 1,
            tooltip: "hide",
            ticks: Array.from(this.props.ticks.keys()),
            ticks_labels: this.props.ticks,
            value: this.state.tick,
        });

        this.slider.on("change", (change) => {
            window.hour = this.props.ticks[change.newValue];

            window.assignmentLayer.getSource().clear();
            window.addFeatures(assignments[hour], assignmentLayer);

            window.absoluteLayer.getSource().clear();
            window.addFeatures(absolute[hour], absoluteLayer);
        });


        var queryInterval = 3500;
        setInterval(this.next.bind(this), queryInterval);
    }

    play(e){
        this.setState({playing: !this.state.playing}); //Toggle playing
    }

    next(){
        if(this.state.playing) {
            this.state.tick = (this.state.tick+1)%this.props.ticks.length;
            this.slider.setValue(this.state.tick, true, true);
        }
    }

    render(){
      return (
        <div className="map-slider">
          <button className="btn btn-default btn-circle btn-lg" onClick={(e) => {this.play(e);}}>
            <i className={"glyphicon " + (this.state.playing ? 'glyphicon-pause' : 'glyphicon-play')}></i>
          </button>

          <input id="time" type="text" data-provide="slider" data-slider-value={this.state.tick}/>
        </div>
      );
    }
}



class Parameters extends React.Component {

    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);

        this.typingTimer = null;                //timer identifier
    }

    isochrone(){
        // Draw isochrones onchange
        isochroneLayer.getSource().clear();
        jshelper.isochrone(window.params.natransport);
    }

    handleChange(e) {

        if(e.target.name=="natransport"){
            clearTimeout(this.typingTimer);
            this.typingTimer = setTimeout(this.isochrone, 3000);
        }

        console.log(e.target.type);

        var value = e.target.value;
        if(e.target.type=="number") value = Number(e.target.value);

        window.params[e.target.name] = value;
        this.forceUpdate();
    }

    render(){
      return (
        <fieldset className="form-group">
            <legend>Beperkingen</legend>

            <label htmlFor="inputNatransport">Natransport (s)</label>
            <div className="form-group">
              <input name="natransport" type="number" className="form-control" id="inputNatransport"
                     defaultValue={window.params.natransport} onChange={this.handleChange}/>
            </div>

            <label htmlFor="inputBudget">Budget (€)</label>
            <div className="form-group">
              <input name="budget" type="number" className="form-control" id="inputBudget"
                     defaultValue={window.params.budget} onChange={this.handleChange}/>
            </div>
        </fieldset>
        //TODO margin tot weg/ conditioneel
      );
    }
}

class Snapshots extends React.Component {

    constructor(props) {
        super(props);
        this.state = {snapshots: []};
        this.save = this.save.bind(this);
    }


    save(){
        map.once('postcompose', (event) => {
            var canvas = event.context.canvas;
            canvas.toBlob((blob) => {
                console.log(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = () => {
                    this.state.snapshots.push(reader.result);
                    this.forceUpdate();
                }
            });
        });
        map.renderSync();
    }

    show(idx) {
        $("#map").hide();
        $("#time-slider").hide();
        $("#snapshot").attr("src", this.state.snapshots[idx]);
        $("#snapshot").show();
    }

    back() {
        $("#snapshot").hide();

        $("#map").show();
        $("#time-slider").show();
    }

    remove(idx) {
        this.state.snapshots.splice(idx, 1);
        this.forceUpdate();
    }

    render(){
      return (
        <div>
            <button onClick={this.save}>Take snapshot</button>
            <button onClick={this.back}>Back to map</button>

            <div className="container">
                <div className="row">
                {
                    this.state.snapshots.map((snap, idx) => {
                        return (
                            <div key={idx} className="col-xs-4">
                                <a href="#" onClick={()=>{this.show(idx);}}
                                   className="thumbnail" style={{position: 'relative'}}>
                                    <i onClick={()=> {this.remove(idx);}}
                                       className="glyphicon glyphicon-remove"
                                       style={{position:'absolute',top:6,right:5,zIndex:51,color:'red'}}/>
                                    <img id="target" src={snap} alt="..." />
                                </a>
                            </div>
                        )
                    })
                }
                </div>
            </div>
        </div>
      );
    }

}