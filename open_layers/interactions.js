function knipClick() {
    var selectClick = new ol.interaction.Select({
        condition: ol.events.condition.click,
        layers: [knipLayer]
    });
    map.addInteraction(selectClick);
    selectClick.on('select', function(e) {
        e.preventDefault();
        e.stopPropagation();
        console.log(e);

        f = e.target.getFeatures().item(0);
        f_props = f.getProperties();
        f.setProperties({"knip": f_props.knip ? false : true});
        console.log(f_props);
        jshelper.toggleKnip(f_props.s, f_props.e);
    });
}


function createPopup(){
    //Create overlay popup
    var popup = new ol.Overlay({
      element: document.getElementById('popup-container'),
      positioning: 'bottom-center',
      offset: [0, -10]
    });
    map.addOverlay(popup);

    // Show popup onclick
    var selectSingleClick = new ol.interaction.Select({
        multi: false,
        condition: ol.events.condition.singleClick,
        style: (aFeature, aResolution) => {
          window.kak = aFeature.getLayer(map);
          //return aFeature.getLayer(map).getStyleFunction()
        },
    });
    map.addInteraction(selectSingleClick);

    selectSingleClick.on('select',function(e) {
        feature = e.target.getFeatures().item(0);
        popup.setPosition();

        //TODO factory popup method react
        //ReactDOM.render(<Popup />, popup);

        var coords = feature.getGeometry().getCoordinates();
        var features = feature.getProperties();
        test = "<H3>" + features.name + "</H3>";
        var clone = Object.assign({}, features);
        delete clone.name;
        delete clone.geometry;

        test += "<ul>";
        for (c in clone){
            test += "<li><b>" + c + ":</b> "+ clone[c] + "</li>";
        }
        test += "</ul>";


        popup.getElement().innerHTML = test;

        popup.setPosition(coords);
    });
}




/**
 * This is a workaround.
 * Returns the associated layer.
 * @param {ol.Map} map.
 * @return {ol.layer.Vector} Layer.
 */
ol.Feature.prototype.getLayer = function(map) {
    var this_ = this, layer_, layersToLookFor = [];
    /**
     * Populates array layersToLookFor with only
     * layers that have features
     */
    var check = function(layer){
        var source = layer.getSource();
        if(source instanceof ol.source.Vector){
            var features = source.getFeatures();
            if(features.length > 0){
                layersToLookFor.push({
                    layer: layer,
                    features: features
                });
            }
        }
    };
    //loop through map layers
    map.getLayers().forEach(function(layer){
        if (layer instanceof ol.layer.Group) {
            layer.getLayers().forEach(check);
        } else {
            check(layer);
        }
    });
    layersToLookFor.forEach(function(obj){
        var found = obj.features.some(function(feature){
            return this_ === feature;
        });
        if(found){
            //this is the layer we want
            layer_ = obj.layer;
        }
    });
    return layer_;
};