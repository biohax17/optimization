// JS bridge
try {
    var handler = {
      set: function(target, path, value, receiver) {
        // Only update if "top" object in dom
        if(path.length == 2)
            jshelper.update_object(path[0], JSON.stringify(receiver));
      }
    };

    var jshelper;
    new QWebChannel(qt.webChannelTransport, function (channel) {
        jshelper = channel.objects.jshelper;
    });
} catch(err){}


// Global variables
//TODO remove
var center ={"lng": 3.7174243, "lat": 51.0543422};
var iconFeatures=[];
var map;
var shuttles;
var f_id = 0;
var hoverInteraction;

var hour = 14;

$(document).ready(function() {
    // Create the map.
    map = new ol.Map({
        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          })
        ],
        overlays: [],
        target: 'map',
        view: new ol.View({
          center: ol.proj.transform([center.lng, center.lat], 'EPSG:4326', 'EPSG:3857'),
          zoom: 12
        })
    });

    // Add layers
    map.addLayer(eventLayer);
    map.addLayer(assignmentLayer);
    map.addLayer(absoluteLayer);
    map.addLayer(parkingLayer);
    map.addLayer(stopsLayer);
    map.addLayer(isochroneLayer);
    map.addLayer(knipLayer);

    //Add interactions
    knipClick();
    createPopup();
    visualizeFlows([assignmentLayer, absoluteLayer]);

    // create Splitpane
    Split(['#top', '#bottom'], {
        sizes: [60, 40],
        direction: 'vertical',
        onDragEnd: function(e) {
            map.updateSize();
        }
    });
    Split(['#layers', '#map-wrapper'], {
        sizes: [25, 75],
        onDragEnd: function(e) {
            map.updateSize();
        }
    });
    map.updateSize(); //Update size after creating splitpane
    renderComponents();
});

function getId(){
    var tmp = f_id;
    f_id++;
    return tmp;
}

function renderComponents() {
    //Wait for python to set
    if(typeof params === 'undefined' || typeof parkings === 'undefined'){
        setTimeout(renderComponents, 500); //Wait to be set
        return;
    }

    ReactDOM.render(
      React.createElement(Snapshots, {}, null),
      document.getElementById('snapshots')
    );

    timeSlider = ReactDOM.render(
      React.createElement(TimeSlider,
                          {ticks: ["14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00"]},
                           null),
      document.getElementById('time-slider')
    );

    ReactDOM.render(
      React.createElement(LayerList, {layers: map.getLayers().getArray()}, null),
      document.getElementById('layers')
    );

    ReactDOM.render(
      React.createElement(FlowList, {layer: assignmentLayer}, null),
      document.getElementById('flows')
    );

    ReactDOM.render(
      React.createElement(Parameters, {}, null),
      document.getElementById('parameters')
    );

    ReactDOM.render(
      React.createElement(Shuttle, {shuttles: shuttles, parkings: parkings}, null),
      document.getElementById('shuttle')
    );
}



function visualizeFlows(layers){
    // Create Tooltip
    var overlay = new ol.Overlay({
      element: document.getElementById('tooltip'),
      offset: [10, 0],
      positioning: 'bottom-left'
    });
    map.addOverlay(overlay);

    //Highlight on hover
    hoverInteraction = new ol.interaction.Select({
        condition: ol.events.condition.pointerMove, //hover
        layers: layers
    });
    map.addInteraction(hoverInteraction);

    hoverInteraction.on('select', function(e) {
        feature = e.target.getFeatures().item(0);
        tooltip.style.display = feature ? '' : 'none';
        if (feature) {
            overlay.setPosition(e.mapBrowserEvent.coordinate);
            tooltip.innerHTML = feature.get('name');
        }
    });

}

function addFeatures(features, layer, id){
    layer = layer || "n"; //Default value
    var features = new ol.format.GeoJSON().readFeatures(features, { featureProjection: 'EPSG:3857' });

    if (layer==="n"){
        var layer = new ol.layer.Vector({
          title: 'added Layer',
          source: new ol.source.Vector({ features: []}),
        });
        map.addLayer(layer);
    }

    //Give each feature unique id
    features.forEach((f) => {f.setId(getId());});

    layer.getSource().addFeatures(features);
}

function updateTimes(hours, layer){
    hour = Object.keys(hours)[0];
    layer.getSource().clear();
    addFeatures(hours[hour], layer);

    var ticks = Object.keys(hours);
    ticks.sort();


    //Dirty fix
    timeSlider._reactInternalInstance.unmountComponent()
    timeSlider = ReactDOM.render(
      React.createElement(TimeSlider, {ticks: ticks}, null),
      document.getElementById('time-slider')
    );
}

//function updateAbsolute()


function startOptimization(){
    assignmentLayer.getSource().clear();
    absoluteLayer.getSource().clear();
    jshelper.startOptimization($('input[name=method]:checked').val());
}

function showFlows(){
    assignmentLayer.getSource().clear();
    absoluteLayer.getSource().clear();
    jshelper.showFlows();
}


