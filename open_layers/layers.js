// Layer with parking icons
var parkingLayer = new ol.layer.Vector({
    title: 'Parking Layer',
    source: new ol.source.Vector({ features: []}),
    style: new ol.style.Style({
      image: new ol.style.Icon(({
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        opacity: 0.75,
        src: 'images/pr.png'
      }))
    })
});

// Layer with parking icons
var stopsLayer = new ol.layer.Vector({
    title: 'Stops Layer',
    source: new ol.source.Vector({ features: []}),
    style: new ol.style.Style({
      image: new ol.style.Icon(({
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        opacity: 0.75,
        src: 'images/dl.png'
      }))
    })
});

// Layer with flows in absolute numbers
var absoluteLayer = new ol.layer.Vector({
    title: 'Absolute Layer',
    source: new ol.source.Vector({ features: []}),
    style: function(feature){
        if(feature.get("name")>3000){
            w = 8;
            color = "red";
        } else if (feature.get("name")>2000){
            w = 6;
            color = "orange";
        } else if (feature.get("name")>1000){
            w = 4;
            color = "yellow";
        } else {
            w = 2;
            color = "green";
        }

        return new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: color,
            width: w
          })
        });
    }
});

// Layer with assignments to P&R's
var assignmentLayer = new ol.layer.Vector({
    title: 'Assignment Layer',
    source: new ol.source.Vector({ features: []}),
    style: ((feature) => {
        features = this.assignmentLayer.getSource().getFeatures();
        color = Math.round(0xffffff * features.indexOf(feature)/features.length); //Delen door priemgetal

        return new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: "rgba("+ (color >> 16) +","+ (color >> 8 & 255) +","+ (color & 255) +","+ 0.6+")",
            width: 4 //
          })
        });
    })
});

// Layer equal trip times
var isochroneLayer = new ol.layer.Vector({
    title: 'Isochrone Layer',
    source: new ol.source.Vector({ features: []}),
    style: new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'blue',
            width: 3
          }),
          fill: new ol.style.Fill({
            color: 'rgba(0, 0, 255, 0.1)'
          })
    }),
});

// Shows where the event takes place
var eventLayer = new ol.layer.Vector({
    title: 'Event Layer',
    source: new ol.source.Vector({ features: []}),
    style: new ol.style.Style({
      image: new ol.style.Icon(({
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        opacity: 0.9,
        src: 'images/event.png'
      }))
    })
});

// Shows where the event takes place
var knipLayer = new ol.layer.Vector({
    title: 'Knip Layer',
    source: new ol.source.Vector({ features: []}),
    style: function(feature){
        if(feature.get("knip")){
            w = 8;
            color = "red";
        } else {
            w = 4;
            color = "green";
        }

        return new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: color,
            width: w
          })
        });
    }
});