import bisect
import random
from datetime import datetime, timedelta
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import json

"/home/karel/Documents/data/2017_05/ReistijdenSegment/InUitvalsweg15"

def load(way, date):
    #segID = 1519547
    date = datetime.strptime(date, "%d/%m/%y")

    path = "/home/karel/Documents/data/"
    STATIC_DATA = path + "StatischeData/static_Invalsweg{way}.csv"
    TRAVEL_TIME_SEG = path + "{date.year}_{date.month:02d}/ReistijdenSegment/InUitvalsweg{way}/" \
                         "{date.year}{date.month:02d}{date.day:02d}.csv"
    TRAVEL_TIME_WAY = path + "{date.year}_{date.month:02d}/Reistijden/InUitvalsweg{way}/" \
                         "{date.year}{date.month:02d}{date.day:02d}.csv"

    static = STATIC_DATA.format(way=way)
    #seg = TRAVEL_TIME_SEG.format(date=date, way=way)
    way = TRAVEL_TIME_WAY.format(date=date, way=way)


    # Load static info on selected way
    # lengthmm	optimalspeedkph
    static_df = pd.read_csv(static, sep=",")
    #seg_df = static_df.loc[static_df["SegmentID"]==segID]
    #optimalSpeed = seg_df["optimalspeedkph"].iloc[0]    # [ms]
    #segLen = seg_df["lengthmm"].iloc[0]                 # [mm]
    #print("Segment: ", optimalSpeed, segLen)

    # Load segment data
    #st = pd.read_csv(seg, sep=";")
    #print(st.to_string())

    #seg = st.loc[st["SegmentId"]==segID]
    #seg['abs'] = seg['TravelTimeMs'].apply(lambda t: calc_vehicles(t, segLen, optimalSpeed))
    #seg['delta'] = seg['abs'].diff().abs()

    # Load way data
    way_pd = pd.read_csv(way, sep=";")
    static_df["optimal_time_s"] = (static_df["lengthmm"]/1000) / (static_df["optimalspeedkph"]/3.6)
    lanes = static_df.iloc[0]["lanes"]
    wayLen = static_df["lengthmm"].sum()
    optimalSpeed = 3.6 * (static_df["lengthmm"].sum()/1000) / static_df["optimal_time_s"].sum()

    # Edit old, non-consistent pandas
    way_pd = way_pd.rename(index=str, columns={"travel time (ms)": "TravelTimeMs"})

    #show(way_pd, "TravelTimeMs")

    way_pd['rep'] = way_pd['TravelTimeMs'].apply(lambda t: int(t > 1000*static_df["optimal_time_s"].sum()+1.2*way_pd['TravelTimeMs'].std()))
    way_pd["rep"] = pd.rolling_mean(arg=way_pd['rep'], window=3, center=True, min_periods=1)

    way_pd["TravelTimeMs"] = pd.rolling_mean(arg=way_pd['TravelTimeMs'], window=4)
    way_pd['abs'] = way_pd['TravelTimeMs'].apply(lambda t: calc_vehicles(t, wayLen, optimalSpeed, lanes))
    way_pd['abs'] = way_pd['rep']*way_pd['abs']
    way_pd["abs"] = way_pd["abs"].fillna(value=.2 * way_pd["abs"].std())

    #way_pd['delta'] = way_pd['abs'].diff().abs()

    way_pd["rol_mean"] = pd.rolling_mean(arg=way_pd['abs'], window=6, center=True, min_periods=3)

    #way_pd["rol_mean"] = pd.rolling_mean(arg=way_pd['delta'], window=4, center=True)
    #way_pd['delta'] = way_pd['rol_mean'].diff().abs()

    return way_pd["rol_mean"]


def calc_vehicles(time, length, optimal_speed, lanes=2):
    speed = 3.6 * (length / time)

    # Fundamental diagram
    # Intensity q [vrt/u]
    # Density k [vrt/km]

    # Capacity: max intensity [pae/u]
    n = bisect.bisect_left([50, 70, 90], optimal_speed)
    road_cap = [650, 1200, 1800]
    q_c = road_cap[n] * lanes
    k_c = q_c / optimal_speed

    # Verzadiging: volledige baan vol met wagens
    q_j = 0
    k_j = calc_k_j()

    # Op elk moment in fund diagram: q = k*u (u = snelheid in kph)
    q = (speed * k_j) / (1 - speed * (k_c - k_j)/(q_c - q_j))
    return q * 0.25     # Per kwartier


def calc_k_j(avg_car_length=4.8, avg_car_dist=1.6):
    return 1000 / (avg_car_length + avg_car_dist)


def show(df, col):
    fig, ax = plt.subplots()
    ax.plot(df["timestamp"], df[col])
    ax.set_xticks(df["timestamp"])
    #ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))
    #ax.xaxis.set_minor_formatter(mdates.DateFormatter("%Y-%m"))
    _ = plt.xticks(rotation=90)
    plt.show()


def map_data(start_date):

    #key = gents ar
    ar_map = {
        "1": {
            # N9 Belzele
            "in": [{"way": 11, "pct": 1.0}],
        },
        "2": {
            # R4 Evergem & N458 Wondelgem
            "in": [{"way": 13, "pct": .72}, {"way": 14, "pct": .28}],
        },
        #"3": {
        #    "out": [],
        #},
        #"4": {
        #    "out": [],
        #},
        "5": {
            # R4 oostakker & N456 Evergem
            "in": [{"way": 0, "pct": .63}, {"way": 12, "pct": .37}],
        },
        "6": {
            # N70 Lochristi
            "in": [{"way": 1, "pct": 0.0}],
        },
        "7": {
            # N70 Lochristi
            "in": [{"way": 1, "pct": 1.0}],
        },
        "8": {
            # N445 Destelbergen
            "in": [{"way": 2, "pct": 1.0}],
        },
        "9": {
            # N9 Melle
            "in": [{"way": 4, "pct": 1.0}],
        },
        #"10": {
        #    # Out N9 Melle
        #    "out": []
            #"out": [{"way": 4, "pct": 1.0}],
        #},
        "11": {
            # In: E40 bxl, E40 oost, E17 ant, E17 kortrijk
            #"in": [{"way": 5, "pct": 1}, {"way": 9, "pct": 1}, {"way": 3, "pct": 1}, {"way": 6, "pct": 1}],
            "in": [{"way": 5, "pct": .67}, {"way": 9, "pct": .61}, {"way": 3, "pct": .48}, {"way": 6, "pct": .51}],
        },
        #"12": {
        #    # Out van 11
        #    "out": []
        #    # "out": [{"way": 4, "pct": 1.0}],
        #},
        "13": {
            # N60 zwijnaarde & N43 Sint Denijs
            "in": [{"way": 7, "pct": 0.84}, {"way": 8, "pct": 0.16}]
        },
        #"14": {
        #    # ouN60 zwijnaarde & N43 Sint Denijs
        #    "out": []
        #},
        "15": {
            # N43 Sint Denijs
            "in": [{"way": 8, "pct": .72}]
        },
        "18": {
            # N466 Drongen
            #"in": [{"way": 10, "pct": 1}] juist feitlijk
            "in": [{"way": 10, "pct": .74}]

        },
    }

    df = pd.DataFrame()
    start_pd = pd.to_datetime(start_date, format='%d/%m%/%y', errors='ignore')
    df['Timestamp'] = pd.date_range(start=start_pd, periods=(24*60)/15, freq="15min")
    df = df.set_index('Timestamp')

    for way in range(20):
        if str(way) in ar_map:
            dag_weg_pd = load(str(way), start_date)
            kwargs = {str(way): pd.Series(dag_weg_pd).values}
            df = df.assign(**kwargs)

    print(df.to_string())

    # Create empty copy
    input_df = pd.DataFrame(index=df.index)

    for ar in ar_map.keys():
        for mapping in ar_map[ar]["in"]:
            if mapping["way"] in input_df.columns:
                input_df[mapping["way"]] += df[ar].multiply(mapping["pct"])#

            else:
                #input_df[mapping["way"]] = mapping["pct"] * df[ar]
                input_df[mapping["way"]] = mapping["pct"] * df[ar]


    # Noise on: E40 bxl, E40 oost, E17 ant, E17 kortrijk
    for idx, way in enumerate((5, 9, 3, 6)):
        #input_df[way] = pd.rolling_mean(arg=input_df[way], window=10, center=True, min_periods=3)
        print(input_df[way].std())
        print(input_df[way].mean())

        input_df[way] = input_df[way].apply(
            lambda t: t+random.randint(int(1.2 * -input_df[way].std()), int(1.2 * input_df[way].std())) if t > 0 else t)
        #input_df[way] *= ar_map["11"]["spec"][idx]["pct"]


        input_df[way] = pd.rolling_mean(arg=input_df[way], window=2, center=True, min_periods=1) #verstoring smoothen
        input_df[way] = pd.rolling_mean(arg=input_df[way], window=8, center=True, min_periods=1)
        input_df[input_df < 0] = 0 # filter neg values
        #input_df[way] *= np.random.uniform(low=1, high=1.5, size=df.shape[0])


    input_df = input_df.round()
    return input_df

if __name__ == "__main__":
    # Creates list of roads from json
    with open('../data/accessroads.json', 'r') as roadsfile:
        json_roads = json.load(roadsfile)

    start_date = "15/07/17"
    #start_date = "6/05/17"
    start_date = "14/10/17"

    #demand = map_data(start_date)
    #demand.to_csv("../data/demand0.csv",)
    demand = pd.read_csv("../data/150717.csv", parse_dates=True, index_col=0)

    # Identifying most visited timewindow
    # Window van 6 uur voorzien
    demand["total"] = demand.sum(axis=1)

    hours = 6
    win = hours * 4
    demand["cum"] = pd.rolling_sum(arg=demand["total"], window=win, center=False)
    print(demand.to_string())

    window_end = demand["cum"].idxmax(0)
    max_window = demand.loc[window_end - pd.Timedelta(hours=hours): window_end]
    print(max_window.to_string())

    for col in max_window.columns:
        print(col, ": ", int(max_window[col].sum()*.45))

    #Scale to hours
    hours = max_window.groupby([max_window.index.hour]).sum()
    print(hours.to_string())
    print(hours.to_dict())




    # Human readable plot
    demand = demand.rename(columns={str(v): k["name"] for v, k in enumerate(json_roads)})
    print(demand.to_string())
    demand.drop(['total', 'cum'], axis=1).plot()
    plt.show()