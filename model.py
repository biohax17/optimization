import json
import pickle

#from PySide.QtCore import QObject
from PyQt5.QtCore import QObject

import routingutils, pprint
import networkx as nx

class Location(QObject):
    def __init__(self, lat, lng, *args, **kwargs):
        super(Location, self).__init__(*args, **kwargs)
        self.lat = lat
        self.lng = lng


class AccessRoad(Location):
    def __init__(self, name, demand, *args, **kwargs):
        super(AccessRoad, self).__init__(*args, **kwargs)
        self.name = name
        self.demand = demand

    def __str__(self):
        return self.name


class Parking(Location):
    def __init__(self, name, capacity, price=0, shuttle="0", *args, **kwargs):
        super(Parking, self).__init__(*args, **kwargs)
        self.name = name
        self.capacity = capacity
        self.initial_capacity = capacity
        self.price = price
        self.used = False
        self.shuttle = shuttle

    def rest_capacity(self):
        return self.capacity-self.initial_capacity

    def __str__(self):
        return self.name


class Shuttle(object):
    def __init__(self, duration, cost=0):
        self.duration = duration
        self.cost = cost

    def cost(self, km, duration_u, km_cost=0.7, wage_u=30, wage_tax=0.06, shuttle_tax=0.06):
        cost_driver = (1 + wage_tax) * (wage_u * duration_u)
        cost_shuttle = (1 + shuttle_tax) * (km_cost * km)
        return cost_driver + cost_shuttle



class HashableGraph(nx.MultiDiGraph):
    def __init__(self, *args, **kwargs):
        super(HashableGraph, self).__init__(*args, **kwargs)

    def __hash__(self):
        h = hash(pickle.dumps(nx.json_graph.adjacency_data(self), protocol=pickle.HIGHEST_PROTOCOL))
        return h