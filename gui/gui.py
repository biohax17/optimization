import sys
from PySide.QtCore import QObject, Slot, QDir, QUrl
from PySide.QtGui import QApplication
from PySide.QtWebKit import QWebView

from model import *
import json

html = """
<html>
<body>
    <h1>Hello!</h1><br>
    <h2><a href="#" onclick="printer.text('Message from QWebView')">QObject Test</a></h2>
    <h2><a href="#" onclick="alert('Javascript works!')">JS test</a></h2>
</body>
</html>
"""


class ConsolePrinter(QObject):
    def __init__(self, parent=None):
        super(ConsolePrinter, self).__init__(parent)

    @Slot(str)
    def text(self, message):
        print(message)

class MapView(QWebView):
    def __init__(self, center, G, access_roads, parking_candidates, shuttles):
        QWebView.__init__(self)

        self.center = center
        self.G = G
        self.access_roads = access_roads
        self.parking_candidates = parking_candidates
        self.shuttles = shuttles

        self.page().mainFrame().addToJavaScriptWindowObject('center', center)
        #self.page().mainFrame().addToJavaScriptWindowObject('parking_candidates', parking_candidates)

        self.current_url = ''

        path = QDir.current().filePath('../open_layers/index.html')
        self.load(QUrl.fromLocalFile(path))


if __name__ == '__main__':
    # Center partyzone
    center = Location(lat=51.057002, lng=3.728418)  # Charla

    # Creates list of parkings from json
    with open('../data/parkings.json', 'r') as parkingfile:
        json_parkings = json.load(parkingfile)

    # Convert to Parking objects
    parking_candidates = []
    for jp in json_parkings:
        parking_candidates.append(Parking(**jp))

    # Creates list of roads from json
    with open('../data/accessroads.json', 'r') as roadsfile:
        json_roads = json.load(roadsfile)

    # Convert to Road objects
    access_roads = []
    for jr in json_roads:
        access_roads.append(AccessRoad(**jr))

    # Creates dict of shuttles from json
    with open('../data/shuttles.json', 'r') as roadsfile:
        json_shuttles = json.load(roadsfile)

    # Convert to Shuttle objects
    shuttles = {}
    for id, shuttle in json_shuttles.items():
        shuttles[id] = Shuttle(**shuttle)

    # Load graph from pickle
    with open("../graph.pkl", "rb") as input_file:
        G_old = pickle.load(input_file)
        G = HashableGraph(G_old)


    sys.argv.append("--disable-web-security")
    app = QApplication(sys.argv)
    view = MapView(center, G, access_roads, parking_candidates, shuttles)


    # TODO wait for better scaling support
    view.setZoomFactor(2.0 if app.desktop().logicalDpiX() > 96 else 1.0)
    view.show()

    app.exec_()