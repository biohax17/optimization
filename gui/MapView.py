import copy
import pprint
from collections import defaultdict

from PyQt5.QtCore import QDir, QUrl, pyqtSlot


import pandas as pd
from geojson import FeatureCollection, Feature, Point
from multiprocessing.pool import ThreadPool

from gui.SharedQWebView import SharedQWebView
from util.decorators import async

import optimization
import routingutils as ru
from gtfs2graph import get_stops

class MapView(SharedQWebView):
    def __init__(self, G, feed, center, access_roads, parking_candidates, shuttles, demand):
        SharedQWebView.__init__(self)

        self.G = G
        self.feed = feed
        self.center = center
        self.access_roads = access_roads
        self.parking_candidates = parking_candidates
        self.shuttles = shuttles
        self.demand = demand
        self.params = {
            "window": 6,

            "natransport": 1200,
            "budget": 8500,
            #TODO: max_shuttles
        }

        self.current_url = ''
        path = QDir.current().filePath('open_layers/index.html')
        self.load(QUrl.fromLocalFile(path))

        self.loadFinished.connect(self._on_load_finished)

    def _on_load_finished(self):
        self.pass_object("center", self.center)
        self.parking_candidates = self.pass_object("parkings", self.parking_candidates)
        self.shuttles = self.pass_object("shuttles", self.shuttles)
        self.params = self.pass_object("params", self.params)

        #TODO aanzetten voor productie
        @async
        def add_feat():
            self.set_parkings(self.parking_candidates)
            #self.add_features(get_stops(self.feed), "stopsLayer")
            self.add_features(FeatureCollection(features=[Point((self.center.lng, self.center.lat))]), "eventLayer")
            #self.add_features(ru.graph_to_geojson(self.G), "knipLayer")
        add_feat()


    @async
    @pyqtSlot(int)
    def startOptimization(self, method):
        # Reset parkings capacity
        for parking in self.parking_candidates:
            parking.capacity = parking.initial_capacity

        # Identify window with most visitors
        self.demand["total"] = self.demand.sum(axis=1)
        win = self.params["window"] * 4    # uur -> kwartier
        self.demand["cum"] = pd.rolling_sum(arg=self.demand["total"], window=win, center=False)
        window_end = self.demand["cum"].idxmax(0)
        window_start = window_end - pd.Timedelta(hours=self.params["window"])
        max_window = self.demand.loc[window_start: window_end]
        hourly_demand = max_window.resample(rule='60Min', base=window_start.minute).sum()
        hourly_demand.index = hourly_demand.index.strftime('%H:%M')
        hourly_demand = hourly_demand.to_dict()

        flows, rest = optimization.optimize(G=self.G,
                                            center=self.center,
                                            access_roads=self.access_roads,
                                            parking_candidates=self.parking_candidates,
                                            shuttles=self.shuttles,
                                            demand=max_window,
                                            budget=self.params["budget"],
                                            natrans=self.params["natransport"],
                                            objective=method)


        # Handle assignments
        pool = ThreadPool(4)

        def create_flow(f):
            if f["flow"] > 0:
                dest = self.parking_candidates[f["parking"]] if "parking" in f else self.center
                geo = ru.route_osm(self.G, self.access_roads[f["accessroad"]], dest)
                demand_dict = {}

                name = "Flow: {flow:.2f} from {ar} {to}"

                for hour, demand in hourly_demand[self.access_roads[f["accessroad"]]].items():
                    tmp = copy.deepcopy(geo)
                    tmp["properties"] = {
                        "name": name.format(flow=f["flow"],
                                            ar=self.access_roads[f["accessroad"]].name,
                                            to="to " + self.parking_candidates[f["parking"]].name if "parking" in f else ""),
                        "demand": int(demand * f["flow"])
                    }
                    demand_dict[hour] = tmp

                return demand_dict

        assignment_flows = list(filter(lambda x: x is not None, pool.map(create_flow, flows)))
        hour_flows = defaultdict(list)
        for d in assignment_flows:
            for k,v in d.items():
                hour_flows[k].append(v)
        hour_features = {k: FeatureCollection(features=v) for k, v in hour_flows.items()}

        self.pass_object("assignments", hour_features)
        self.page().runJavaScript("updateTimes(assignments, assignmentLayer);")

        # Handling rest flows
        for d in filter(lambda x: x is not None, pool.map(create_flow, rest)):  # Filter nones
            for k,v in d.items():
                hour_flows[k].append(v)
        hour_routes = {h: FeatureCollection(features=feat) for h, feat in hour_flows.items()}

        # Absolute numbers
        hour_abs = {}
        for hour, routes in hour_routes.items():
            hour_abs[hour] = ru.absolute_numbers(routes)
        self.pass_object("absolute", hour_abs)
        self.page().runJavaScript("updateTimes(absolute, absoluteLayer);")

    @pyqtSlot(str, str)
    def toggleKnip(self, s, e):
        # Marchalling error with signed integers, passing as str
        ru.toggle_knip(self.G, int(s), int(e))

    @async
    @pyqtSlot(int)
    def isochrone(self, duration):
        pool = ThreadPool(4)
        def iso(park):
            iso = ru.make_iso_polys(self.G, park, duration)
            self.add_features(iso, "isochroneLayer")
        pool.map(iso, self.parking_candidates)

    @async
    @pyqtSlot()
    def showFlows(self):
        #TODO timedependant
        flows = []

        def test(ar):
            geo, len = ru.route_osm(self.G, ar, self.center)
            geo["properties"] = {
                "name": "flow: " + str(ar.demand) + " from " + ar.name,
                "demand": ar.demand
            }
            flows.append(geo)
            print(ar.demand*len/1000)
            #self.set_route(geo)

        # make the Pool of workers
        pool = ThreadPool(4)
        pool.map(test, self.access_roads)

        # close the pool and wait for the work to finish
        pool.close()
        pool.join()

        test = ru.absolute_numbers(FeatureCollection(features=flows))
        print(test)
        self.add_features(test, "absoluteLayer")


    def set_parkings(self, parkings):
        self.parkings = parkings
        fc = FeatureCollection(features=[])
        for parking in parkings:
            fc["features"].append(
                Feature(
                    geometry=Point((parking.lng, parking.lat)),
                    properties={
                        "name": parking.name,
                        "capacity": parking.capacity
                    }
                )
            )
        print(fc)
        self.add_features(fc, "parkingLayer")

    #TODO
    @pyqtSlot(int)
    def calcShuttle(self):
        print("Called")
        tmp = ru.route_osm_duration(self.G, self.parking_candidates[0], self.center, ru.speed)
        print(tmp)
        return str(tmp)


    def add_features(self, features, layer=None):
        c = "addFeatures(%s%s);" % (str(features), ","+layer if layer else"")
        #print(c)
        self.page().runJavaScript(c)