import json
from json import JSONEncoder
import weakref

import wrapt as wrapt
from PyQt5 import QtWebChannel
from PyQt5.QtCore import pyqtSlot

from PyQt5.QtWebEngineWidgets import QWebEngineView

from model import Shuttle, Parking


class SharedQWebView(QWebEngineView):

    class CustomProxy(wrapt.ObjectProxy):

        def __init__(self, wrapped):
            super(SharedQWebView.CustomProxy, self).__init__(wrapped)

        def update(self, update):
            self.__wrapped__ = update

        #TODO edit in python part, call
        #SharedQWebView.update_js()

    def __init__(self):
        QWebEngineView.__init__(self)

        self.channel = QtWebChannel.QWebChannel(self.page())
        self.page().setWebChannel(self.channel)
        self.inject_proxy_code()
        self.channel.registerObject("jshelper", self)
        self.object_map = weakref.WeakValueDictionary()

    def inject_proxy_code(self):
        code = """
        var createDeepProxy = function(target, handler, name) {
            //https://stackoverflow.com/questions/43177855/how-to-create-a-deep-proxy
            const preproxy = new WeakMap();
        
            function makeHandler(path) {
                return {
                    set(target, key, value, receiver) {
                        if(typeof value === 'object') {
                            value = proxify(value, [...path, key]);
                        }
                        target[key] = value;
        
                        if(handler.set) {
                            handler.set(target, [...path, key], value, receiver);
                        }
                        return true;
                    },
        
                    deleteProperty(target, key) {
                        if(Reflect.has(target, key)) {
                            unproxy(target, key);
                            let deleted = Reflect.deleteProperty(target, key);
                            if(deleted && handler.deleteProperty) {
                                handler.deleteProperty(target, [...path, key]);
                            }
                            return deleted;
                        }
                        return false;
                    }
                }
            }
        
            function unproxy(obj, key) {
                if(preproxy.has(obj[key])) {
                    // console.log('unproxy',key);
                    obj[key] = preproxy.get(obj[key]);
                    preproxy.delete(obj[key]);
                }
        
                for(let k of Object.keys(obj[key])) {
                    if(typeof obj[key][k] === 'object') {
                        unproxy(obj[key], k);
                    }
                }
        
            }
        
            function proxify(obj, path) {
                for(let key of Object.keys(obj)) {
                    if(typeof obj[key] === 'object') {
                        obj[key] = proxify(obj[key], [...path, key]);
                    }
                }
                let p = new Proxy(obj, makeHandler(path));
                preproxy.set(p, obj);
                return p;
            }
    
        return proxify(target, [name]);
        }
        """
        self.page().runJavaScript(code)

    @pyqtSlot(str, str)
    def update_object(self, name, obj):
        print(name, obj)
        obj = json.loads(obj)

        #TODO generiek
        if name == "shuttles":
            obj = {k: Shuttle(**v) for k,v in obj.items()}
        elif name == "parkings":
            obj = [Parking(**p) for p in obj]

        print(obj)

        # Update obj
        self.object_map[name].update(obj)


    def pass_object(self, name, obj):
        #TODO wait for register object to start working
        #self.channel.registerObject(obj)

        obj = self.CustomProxy(obj)
        self.object_map[name] = obj

        class ObjEncoder(JSONEncoder):
            def default(self, o):
                if isinstance(o, list):
                    return list(o)
                elif isinstance(o, dict):
                    return dict(o)
                else:
                    #TODO type hier meegeven anders
                    return o.__dict__

        json_obj = json.dumps(obj, cls=ObjEncoder)
        inst = name + " = createDeepProxy(" + json_obj + ", handler, \"" + name + "\");"

        self.page().runJavaScript(inst)
        #print(inst)
        return obj