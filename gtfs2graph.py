import datetime
import networkx as nx
import pandas as pd
import sys
from geojson import FeatureCollection, Feature, Point
from shapely.geometry import Polygon
import gtfstk as gt


def gent_as_feed():
    path = 'data/DL_gtfs.zip'
    feed_vl = gt.read_gtfs(path, dist_units='km')

    gent = Polygon(
        [(3.659926, 51.111417),
         (3.707991, 51.126071),
         (3.790389, 51.1209),
         (3.831588, 51.102794),
         (3.867293, 51.067426),
         (3.872786, 51.028575),
         (3.828841, 50.996607),
         (3.778029, 50.9724),
         (3.706618, 50.974994),
         (3.647567, 50.987963),
         (3.594008, 51.01389),
         (3.591262, 51.063974)]
    )

    # gent = Polygon([(51.07355,3.87259), (51.12782,3.83114), (51.14763,3.76584), (51.14159,3.70467), (51.10191,3.62214), (51.04399,3.59828), (50.9914,3.62008), (50.96811,3.72711), (50.97594,3.80256), (51.00879,3.84581), ])
    gent = Polygon(
        [(3.87259, 51.07355), (3.83114, 51.12782), (3.76584, 51.14763), (3.70467, 51.14159), (3.62214, 51.10191),
         (3.59828, 51.04399), (3.62008, 50.9914), (3.72711, 50.96811), (3.80256, 50.97594), (3.84581, 51.00879), ])

    #gent = Polygon([(3.728528, 51.057481), (3.728959, 51.055719), (3.727158, 51.056350)])

    feed = gt.restrict_to_polygon(feed_vl, gent)
    return feed

def preprocess_feed(feed):
    feed.stop_times['arrival_time'] = pd.to_timedelta(feed.stop_times.arrival_time, unit="H")
    feed.stop_times['departure_time'] = pd.to_timedelta(feed.stop_times.departure_time, unit="H")


def create_graph(feed, date=datetime.date.today()):
    grouped_st = feed.stop_times.sort_values(["trip_id", "stop_sequence"])[["trip_id", "stop_id", "stop_sequence"]]
    # Create origin destinations pairs
    merged = pd.merge(grouped_st.shift(), grouped_st,
                      left_index=True, right_index=True,  # Join on index
                      suffixes=('_orig', '_dest'))

    # Only those of same trip and add routeid
    merged = merged.loc[merged["trip_id_orig"] == merged["trip_id_dest"]] \
        .merge(feed.trips[["route_id", "trip_id"]], left_on="trip_id_orig", right_on="trip_id")

    segments = merged.groupby(["route_id", "stop_id_orig", "stop_id_dest"], as_index=False).size()

    G = nx.MultiDiGraph()
    for segment, count in segments.items():
        G.add_edge(segment[1], segment[2], route_id=segment[0])

    return G


def create_average_graph(feed):
    preprocess_feed(feed)
    grouped_st = feed.stop_times.sort_values(["trip_id", "stop_sequence"])
    # Create origin destinations pairs
    merged = pd.merge(grouped_st.shift(), grouped_st,
                      left_index=True, right_index=True,  # Join on index
                      suffixes=('_orig', '_dest'))

    # Only those of same trip
    merged = merged.loc[merged["trip_id_orig"] == merged["trip_id_dest"]]
    # Duration
    merged["duration"] = (merged["arrival_time_dest"] - merged["departure_time_orig"]).astype('timedelta64[s]')
    # Create segments with average duration
    segments = merged.groupby(["stop_id_orig", "stop_id_dest"]).mean()["duration"]

    G = nx.DiGraph()
    for index, row in feed.stops.iterrows():
        G.add_node(int(row["stop_id"]), x=round(row["stop_lon"], 6), y=round(row["stop_lat"], 6), osmid=0)

    for segment, avg in segments.items():
        G.add_edge(int(segment[0]), int(segment[1]), duration=avg, public_trans=1)

    return G


def trip_to_segments(feed, trip_id):
    stop_times = feed.stop_times[feed.stop_times['trip_id'] == trip_id]
    segments = []
    for i in range(len(stop_times)-1):
        origin = feed.stops[feed.stops['stop_id'] == stop_times['stop_id'].iloc[i]]
        destination = feed.stops[feed.stops['stop_id'] == stop_times['stop_id'].iloc[i + 1]]

        #start_time = parseTime(stop_times['departure_time'].iloc[i])
        #end_time = parseTime(stop_times['arrival_time'].iloc[i+1])
        #duration = (end_time - start_time).seconds
        #if duration == 0:
        #    speed = 0 # when the laws of physics do not apply
        #else:
        #    speed = distance / duration * 60 * 60
        segments.append({'origin_id': origin['stop_id'].iloc[0],
                         'destination_id': destination['stop_id'].iloc[0],
                        })
    return segments


def parseTime(time): # to deal with times after midnight
    if int(time.split(':')[0]) > 23:
        return datetime.datetime.strptime('2:' + str(int(time.split(':')[0]) - 24) + ':'
                                          + time.split(':')[1] + ':' + time.split(':')[2], '%d:%H:%M:%S')
    else:
        return datetime.datetime.strptime('1:' + time, '%d:%H:%M:%S')


def next_transport_connection(feed, stop_id, route_ids=None, time=datetime.datetime.now()):
    #TODO Route attribute meegeven
    #TODO pass time, calculate waiting time
    time = (time - datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0))
    #print("time_in_next_transport:" + str(time))
    merged = pd.merge(feed.trips, feed.stop_times, on='trip_id')

    if route_ids:
        merged = merged[merged["route_id"].isin(route_ids)]

    # Aansluiting bepalen voor stop
    stop_next = merged[(merged["stop_id"] == stop_id)
                & (datetime.timedelta(days=1) > merged['departure_time'])  # Today
                & (merged['departure_time'] > time)]                       # Yet to come

    # Eerst volgende
    if stop_next.empty: return pd.DataFrame()
    return stop_next.loc[stop_next["departure_time"].idxmin()]


def get_stops(feed):
    stops = FeatureCollection(features=[])
    for index, row in feed.stops.iterrows():
        stops["features"].append(Feature(
            geometry=Point(coordinates=(row["stop_lon"], row["stop_lat"])),
            properties={"name": row["stop_name"], "stop_id": row["stop_id"]}
        ))

    return stops

def speed(feed, time):
    def weight(u,v,d):
        #TODO Wait time
        print(d)
        next = next_transport_connection(feed, u, d["route_id"])
        print(feed.stop_times[feed.stop_times["trip_id"] == next["trip_id"]])
        length = d[0].get('length', 1)
        #parseTime(v)-parseTime(u)

        return next["departure_time"]

    return weight


def speed1(feed, time=None):
    print("wtf")
    def weight(u, v, d):
        # TODO Wait time
        next = next_transport_connection(feed, u, d["route_id"], speed1.time)
        arrival = feed.stop_times[(feed.stop_times["trip_id"] == next["trip_id"])
                                  & (feed.stop_times["stop_id"] == v)].item()

        speed1.time = arrival["departure_time"]
        print(arrival["departure_time"])
        return (arrival["arrival_time"] - next["departure_time"]).seconds

    speed1.time = datetime.datetime.now()


def speed1_gtfs(feed, time=None):
    print("wtf")
    def weight(u, v, d):
        # TODO Wait time
        u_next = next_transport_connection(feed, u, d["route_id"], speed1_gtfs.speed1_gtfs.arrival_times.get(u, datetime.datetime.now()))
        v_arrival = feed.stop_times[(feed.stop_times["trip_id"] == u_next["trip_id"])
                                  & (feed.stop_times["stop_id"] == v)].item()

        speed1_gtfs.arrival_times[v_arrival] = v_arrival["departure_time"]  # TODO maybe route
        print("U dep " +str(u_next["departure_time"]))
        print("V arriv " +str(v_arrival["arrival_time"]))
        return (v_arrival["arrival_time"] - u_next["departure_time"]).seconds

    # Function variable with arrival times
    speed1_gtfs.arrival_times = {}


if __name__ == "__main__":
    print("Creating feed")
    feed = gent_as_feed()
    print("Feed created")
    G = create_graph(feed)
    print("Graph created")
    for nr in nx.shortest_path(G, "114092", "111361", weight=speed1(feed)):
        print(feed.stops[feed.stops["stop_id"] == nr]["stop_name"])