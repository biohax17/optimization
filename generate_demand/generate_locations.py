# coding: utf-8
import pandas as pd
import numpy as np
import random
from geopy.distance import vincenty
import itertools
#from mpl_toolkits.basemap import Basemap
#import matplotlib.pyplot as plt

# In[ ]:
"""
def plotSimul(SimulMat):
  NumSimuls=SimulMat.shape[0]
  map = Basemap(projection='merc',
		llcrnrlat=50,urcrnrlat=52,
		llcrnrlon=2,urcrnrlon=6.5,lat_ts=20,resolution='h')
  map.drawcoastlines()
  map.drawcountries()
  map.fillcontinents(color = 'coral')
  map.drawmapboundary()
  for i in range(NumSimuls):
        x,y = map(SimulMat[i][1], SimulMat[i][0])
        map.plot(x, y, 'bo', markersize=1)
  plt.show()
"""
# In[ ]:
# read the param files
# to be replaced by GUI

#Params = pd.read_excel('GenLocParams.xlsx','Params')
#Params.index = Params.Id
#EventLat = Params.loc['EventLat','Param']
#EventLong = Params.loc['EventLong','Param']
#EventLoc=(EventLat,EventLong)

# NumberOfSimuls must be a 4-tuple for antithetic simulation
#NumberOfSimuls= int(4*int(Params.loc['NumberOfSimuls','Param']/4))
#PartipMinDistance= Params.loc['PartipMinDistance','Param']
#PartipMaxDistance= Params.loc['PartipMaxDistance','Param']

# In[ ]:
# The world is flat around the event
# we measure the length of 0.01 lat and Long
"""
EventLocPlusLat = (EventLat+0.01,EventLong)
EventLocPlusLong = (EventLat,EventLong+0.01)
LatDist = vincenty(EventLocPlusLat, EventLoc).kilometers
LongDist = vincenty(EventLocPlusLong, EventLoc).kilometers
"""

"""
# In[ ]:
# homogenous density around Event
Opp=np.pi * (PartipMaxDistance*PartipMaxDistance - PartipMinDistance*PartipMinDistance)

# build in some margin for rounding effects
GridSize=np.sqrt(Opp/(NumberOfSimuls)*0.98)

# build simulations
HomogSimulLocs=np.zeros((NumberOfSimuls+500, 2))
i=0
NumberGrids=int(round(PartipMaxDistance/GridSize ))
for x in range(-NumberGrids, NumberGrids):
    for y in range(-NumberGrids, NumberGrids):
        if ((np.sqrt(x*x+y*y))*GridSize<PartipMaxDistance) and ((np.sqrt(x*x+y*y))*GridSize>PartipMinDistance):
            HomogSimulLocs[i][0]=EventLat+x*GridSize*0.01/LatDist
            HomogSimulLocs[i][1]=EventLong+y*GridSize*0.01/LongDist
            i=i+1
np.savetxt("HomogSimuls.csv", HomogSimulLocs, delimiter=",")
plotSimul(HomogSimulLocs)

# In[ ]:
# homogenous density around Event

# build simulations
HomogSimulLocs=np.zeros((NumberOfSimuls, 2))
i=0
while (i < NumberOfSimuls):
    x=-PartipMaxDistance+random.random()*2*PartipMaxDistance
    y=-PartipMaxDistance+random.random()*2*PartipMaxDistance
    if ((np.sqrt(x*x+y*y))<PartipMaxDistance) and ((np.sqrt(x*x+y*y))>PartipMinDistance):
            HomogSimulLocs[i][0]=EventLat+x*0.01/LatDist
            HomogSimulLocs[i][1]=EventLong+y*0.01/LongDist
            i=i+1

np.savetxt("HomogSimuls.csv", HomogSimulLocs, delimiter=",")
plotSimul(HomogSimulLocs)

# In[ ]:
# linear decreasing density around Event

NumberOfRings= int(round(np.sqrt (NumberOfSimuls*1.02/(2*np.pi))))
NumberOfLocsOnRing=int(round(2*NumberOfRings*np.pi))
LinDecreasSimulLocs=np.zeros((NumberOfSimuls+500, 2))
i=0
for j in range(NumberOfRings):
    for k in range(NumberOfLocsOnRing):
        r=(j)*(PartipMaxDistance-PartipMinDistance)/NumberOfRings+PartipMinDistance
        teta=0+k*(2*np.pi/NumberOfLocsOnRing)
        x=r*np.cos(teta)
        y=r*np.sin(teta)
        LinDecreasSimulLocs[i][0]=EventLat+x*0.01/LatDist
        LinDecreasSimulLocs[i][1]=EventLong+y*0.01/LongDist
        i=i+1

np.savetxt("LinDecSimuls.csv", LinDecreasSimulLocs, delimiter=",")        
plotSimul(LinDecreasSimulLocs)
"""
# In[ ]:
# linear decreasing density around Event Monte Carlo
def monte_carlo(event, visitors, min_dist, max_dist):
    EventLocPlusLat = (event.lat+ 0.01, event.lng)
    EventLocPlusLong = (event.lat, event.lng + 0.01)
    LatDist = vincenty(EventLocPlusLat, (event.lat,event.lng)).kilometers
    LongDist = vincenty(EventLocPlusLong, (event.lat,event.lng)).kilometers

    NumberOfSimuls= int(4*int(visitors/4))

    locations = np.zeros((NumberOfSimuls, 2))
    NumberOfS4=int(NumberOfSimuls/4)
    for i in range(NumberOfS4):
            j=i*4
            r = min_dist + random.random()*(max_dist-min_dist)
            teta=random.random()*2*np.pi
            x=r*np.sin(teta)
            y=r*np.cos(teta)
            locations[j][0]=event.lat + x*0.01/LatDist
            locations[j][1]=event.lng + y*0.01/LongDist
            teta = teta +np.pi/2
            x=r*np.sin(teta)
            y=r*np.cos(teta)
            locations[j+1][0] = event.lat + x*0.01/LatDist
            locations[j+1][1] = event.lng + y*0.01/LongDist
            teta = teta +np.pi/2
            x=r*np.sin(teta)
            y=r*np.cos(teta)
            locations[j+2][0] = event.lat + x*0.01/LatDist
            locations[j+2][1] = event.lng + y*0.01/LongDist
            teta = teta +np.pi/2
            x=r*np.sin(teta)
            y=r*np.cos(teta)
            locations[j+3][0] = event.lat + x*0.01/LatDist
            locations[j+3][1] = event.lng + y*0.01/LongDist
    return locations
    #np.savetxt("LinDecSimuls.csv", locations , delimiter=",")
    #plotSimul(locations)

def distance(p0, p1):
    return np.math.sqrt((p0[0] - p1[0]) ** 2 + (p0[1] - p1[1]) ** 2)

def on_line(c,line):
    #print(line)
    for step in line["steps"]:
        pl = decode_polyline(step["polyline"]["points"])
        #print(pl)
        for link in pairwise(pl):
            #print(link[0])
            return abs(distance(link[0], (c.lat, c.lng))\
                   + distance((c.lat, c.lng), link[1])\
                   - distance(link[0], link[1])) \
                   < 0.1

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)

def decode_polyline(polyline_str):
    index, lat, lng = 0, 0, 0
    coordinates = []
    changes = {'latitude': 0, 'longitude': 0}

    # Coordinates have variable length when encoded, so just keep
    # track of whether we've hit the end of the string. In each
    # while loop iteration, a single coordinate is decoded.
    while index < len(polyline_str):
        # Gather lat/lon changes, store them in a dictionary to apply them later
        for unit in ['latitude', 'longitude']:
            shift, result = 0, 0

            while True:
                byte = ord(polyline_str[index]) - 63
                index+=1
                result |= (byte & 0x1f) << shift
                shift += 5
                if not byte >= 0x20:
                    break

            if (result & 1):
                changes[unit] = ~(result >> 1)
            else:
                changes[unit] = (result >> 1)

        lat += changes['latitude']
        lng += changes['longitude']

        coordinates.append((lat / 100000.0, lng / 100000.0))

    return coordinates