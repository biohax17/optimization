import json

import osmnx as ox, networkx as nx, geopandas as gpd, matplotlib.pyplot as plt
from geojson import FeatureCollection, Point, Feature, LineString

from generate_demand.generate_locations import *
from model import *
import geojson


def simulate():
    ox.config(log_console=True)

    center = Location(lat=51.057002, lng=3.728418)  # Charla
    # Creates list of roads from json
    with open('accessroads.json', 'r') as roadsfile:
        json_roads = json.load(roadsfile)

    #pprint.pprint(json_roads)

    src = monte_carlo(center, 8, 8, 40)  # random punten genereren

    # Open the graph pickle
    with open("../data/vlaanderen.pickle", "rb") as input_file:
        G = pickle.load(input_file)

    G = nx.read_shp('/home/karel/PycharmProjects/Parking optimization/generate_demand/data/vlaanderen.shp/edges/edges.shp')

    #center_node = ox.get_nearest_node(G, (center.lat, center.lng))
    #subgraph = nx.ego_graph(G, center_node, radius=23*60, distance=routingutils.speed1)
    nx.draw_networkx(G)

    #ox.plot_graph(subgraph)
    #ox.save_graph_shapefile(G, filename='vlaanderen.shp')


    #ox.make_shp_filename("")
    #nx.write_shp(G, "data")

    #ox.save_graphml(G,"vlaanderen.graphml")
    #G = ox.load_graphml("vlaanderen.graphml")


    # find the centermost node

    """
    # Preprocessing
    for ar in json_roads:
        ar["demand"] = 0    # Reset
        ar["node"] = ox.get_nearest_node(G, (ar["lat"], ar["lng"]))

    routes = []

    for s in src:
        start_node = ox.get_nearest_node(G, (s[1], s[0]))
        try:
            route = nx.shortest_path(G, start_node, center_node, weight=routingutils.speed1)
            routes.append(LineString(geometry=[Point((G.nodes[node]["x"], G.nodes[node]["y"])) for node in route]))
            for ar in json_roads:
                if ar["node"] in route:
                    ar["demand"] += 1

        except nx.NetworkXNoPath:
            print(s)

    df = FeatureCollection([Feature(geometry=Point((G.nodes[n["node"]]["x"], G.nodes[n["node"]]["y"])),
                                    properties={"name": n["name"], "demand": n["demand"]}) for n in json_roads])

    sf = FeatureCollection([Feature(geometry=Point((s[1], s[0]))) for s in src])

    routes = FeatureCollection(routes)

    return df, sf, routes
    """
if __name__ == "__main__":
    simulate()