from functools import lru_cache
from multiprocessing.pool import ThreadPool

from enum import Enum

from geopy import distance
import googlemaps, pprint
import osmnx as ox, networkx as nx, matplotlib.cm as cm, pandas as pd, numpy as np
from shapely import geometry, ops
from geojson import Point, LineString, Feature, FeatureCollection
import json
import sys

from joblib import Memory
memory = Memory(cachedir="data/cache/", verbose=0)

from util.decorators import freeze

google_key = "AIzaSyCMN_p0F1ohTzf3Tr1UWhamFB4869G9nwc"
ox.config(log_console=False, use_cache=True)

class TransportType(Enum):
    CAR = 1
    FOOT = 2
    PUBLIC = 3


def gent_as_graph():
    ox.config(log_console=True, use_cache=True)

    # download the street network
    #G = ox.core.graph_from_point((51.055262, 3.726480), distance=12000, network_type='drive', simplify=False)
    #ox.save_graphml(G, "gent.graphml")
    G = ox.load_graphml("gent.graphml")

    return G


def concat_graphs(osm_graph, gtfs_graph, acceptable_dist=45):
    G = osm_graph.copy()
    G.add_nodes_from(gtfs_graph.nodes(data=True))
    G.add_edges_from(gtfs_graph.edges(data=True))

    #TODO check if no intersections: possible bug
    #print(list(set(osm_graph.nodes).intersection(gtfs_graph.nodes)))

    for node, data in gtfs_graph.nodes(data=True):
        osm_node, dist = ox.get_nearest_node(osm_graph, (data["y"], data["x"]), return_dist=True)
        if dist < acceptable_dist:
            G.add_edge(node, osm_node, length=dist)
            G.add_edge(osm_node, node, length=dist) #Bidi

    return G

def length_osm_path(G, path):
    """
    Length edge G.edges[(OSM_node1, OSM_node2, 0)][length]
    zip(path, path[1:]) tuple van elt en zijn opeenvolgende
    """
    return sum(G.edges[e+(0,)]["length"] for e in zip(path, path[1:]))

#@freeze
@memory.cache
def distance_matrix_osm(G, src, dest):
    time_matrix = pd.DataFrame(index=src, columns=dest)

    # Preprocessing
    for s in src:
        s.node = ox.get_nearest_node(G, (s.lat, s.lng))

    for d in dest:
        d.node = ox.get_nearest_node(G, (d.lat, d.lng))

    def all_destinations(s):
        #s_node = ox.get_nearest_node(G, (s.lat, s.lng))
        all_dest = nx.single_source_dijkstra_path(G, source=s.node, weight=speed)
        for d in dest:
            #d_node = ox.get_nearest_node(G, (d.lat, d.lng))
            try:
                time_matrix.loc[s, d] = length_osm_path(G, all_dest[d.node])
            except nx.NetworkXNoPath:
                time_matrix.loc[s, d] = float("inf")

    # make the Pool of workers
    pool = ThreadPool(5)
    pool.map(all_destinations, src)

    # close the pool and wait for the work to finish
    pool.close()
    pool.join()

    return time_matrix

@memory.cache
def route_osm_duration(G, a, b, w_func):
    origin_node = ox.get_nearest_node(G, (a.lat, a.lng))
    destination_node = ox.get_nearest_node(G, (b.lat, b.lng))
    try:
        length = nx.shortest_path_length(G, origin_node, destination_node, weight=w_func)
    except nx.NetworkXNoPath:
        length = float("inf")
    return length


@memory.cache
def route_osm(G, a, b):
    origin_node = ox.get_nearest_node(G, (a.lat, a.lng))
    destination_node = ox.get_nearest_node(G, (b.lat, b.lng))

    try:
        route = nx.shortest_path(G, origin_node, destination_node, weight=speed)
    except nx.NetworkXNoPath:
        route = []

    points = [Point((a.lng, a.lat))]
    for n in route:
        node = G.nodes()[n]
        points.append(Point((node["x"], node["y"])))
    points.append(Point((b.lng, b.lat)))

    return Feature(geometry=LineString(coordinates=points))


@freeze
@memory.cache
def along_path_matrix(G, access_roads, parking_candidates, center, margin):
    a = pd.DataFrame(0, index=access_roads, columns=parking_candidates)

    def feas(ar):
        route = route_osm(G, ar, center)
        for p in parking_candidates:
            a.loc[ar, p] = int(along_path(p, route, margin=margin))

    # make the Pool of workers
    pool = ThreadPool(5)
    pool.map(feas, access_roads)

    # close the pool and wait for the work to finish
    pool.close()
    pool.join()

    return a

@memory.cache
def natras_feas_matrix(G, access_roads, parking_candidates, shuttles, center, natrans):
    print(type(shuttles))
    # Create acceptance matrix (also with shuttle)
    a = pd.DataFrame(0, index=access_roads, columns=parking_candidates)
    a_shuttle = pd.DataFrame(0, index=access_roads, columns=parking_candidates)

    def feas(p):
        feas = int(route_osm_duration(G, p, center, speed_na) <= natrans)
        a[p] = feas
        if not feas and (p.shuttle != "0"):  # Try shuttle if available
            shut_feas = 1 if shuttles[p.shuttle].duration <= natrans else 0
            a_shuttle[p] = shut_feas

    # make the Pool of workers
    pool = ThreadPool(5)
    pool.map(feas, parking_candidates)

    # close the pool and wait for the work to finish
    pool.close()
    pool.join()

    return a, a_shuttle


def super_speed(u, v, d):
    if "public_trans" in d[0]:
        return d[0]["duration"]


def speed_na(u, v, d):
    """Function to pass to networkx algoritm"""
    if "public_trans" in d[0]:
        return d[0]["duration"]

    return ped_speed(u,v,d)


def speed(u, v, d):
    """Function to pass to networkx algoritm"""
    if "public_trans" in d[0]:
        return float("inf")

    if d[0].get("knip", False):
        return float("inf")

    length = d[0].get('length', 1)
    speed = float(d[0].get('maxspeed', '50').split(";")[0])   # niet gekend => 50

    return length/speed

def speed1(u, v, d):
    """Function to pass to networkx algoritm"""
    length = d[0].get('length', 1)
    try:
        speed = float(d[0].get('maxspeed')[0])
    except TypeError:
        speed = 70  # niet gekend => 70
    except ValueError:
        speed = 70

    return length/speed


def ped_speed(u, v, d):
    length = d[0].get('length', 1)
    return length/1.66


def along_path(loc, path, margin=800):
    point = geometry.Point(loc.lng, loc.lat)
    line = geometry.shape(path["geometry"])
    # Find shortest point on path
    sp = line.interpolate(line.project(point))

    return int(distance.vincenty((loc.lng, loc.lat), sp.coords).m <= margin)


def absolute_numbers(routes):
    d = {0: [], 1: [], 2: [], 3: [], 'count': [], 'path': []}
    df = pd.DataFrame(data=d)

    for idx, route in enumerate(routes["features"]):
        cnt = route["properties"].get("demand", 1)

        #df1 = pd.DataFrame.from_dict(xi['geometry']['coordinates'])
        coordinates = route['geometry']['coordinates']
        c = [[x[0], x[1], y[0], y[1]] for x, y in zip(coordinates, coordinates[1:])]
        df1 = pd.DataFrame.from_dict(c)
        df1['count'] = cnt
        df1['path'] = idx
        df = df.append(df1, ignore_index=False)

    # In[Aggregate]:
    agg = df.groupby([0, 1, 2, 3]).agg({0: 'first', 1: 'first', 2: 'first', 3: 'first', 'count': sum, 'path': 'first'})

    fc = FeatureCollection(features=[])
    grouped = agg.groupby('count')
    for name, group in grouped:
        lines = [geometry.LineString([[e[0], e[1]], [e[2], e[3]]]) for e in group.values.tolist()]
        f = Feature(geometry=ops.linemerge(geometry.MultiLineString(lines)), properties={"name": name})
        if f["geometry"]["type"] == "MultiLineString":
            for line in ops.linemerge(geometry.MultiLineString(lines)):
                fc["features"].append(Feature(geometry=line, properties={"name": name}))
        else:
            fc["features"].append(f)

    return fc



"""
def absolute_numbers(routes, weight=1):
    d = {0: [], 1: [], 2: [], 3: [], 'count': [], 'path': []}
    df = pd.DataFrame(data=d)

    fc = FeatureCollection(features=[])
    for idx, route in enumerate(routes["features"]):
        if 'demand' in route["properties"].keys():
            for hour, cnt in route["properties"]["demand"].items():
                #df1 = pd.DataFrame.from_dict(xi['geometry']['coordinates'])
                coordinates = route['geometry']['coordinates']
                c = [[x[0], x[1], y[0], y[1]] for x, y in zip(coordinates, coordinates[1:])]
                df1 = pd.DataFrame.from_dict(c)
                df1['hour'] = hour
                df1['count'] = int(cnt)
                df1['path'] = idx
                df = df.append(df1, ignore_index=False)

    # In[Aggregate]:
    agg = df.groupby([0, 1, 2, 3, "hour"]).agg({0: 'first', 1: 'first', 2: 'first', 3: 'first', 'count': sum, 'path': 'first', 'hour': 'first'})

    #print(agg.to_string())

    grouped = agg.groupby(['hour', 'count'])

    for name, group in grouped:
        print(name)
        lines = [geometry.LineString([[e[0], e[1]], [e[2], e[3]]]) for e in group.values.tolist()]
        f = Feature(geometry=ops.linemerge(geometry.MultiLineString(lines)), properties={"name": name[1], "hour": name[0]})
        if f["geometry"]["type"] == "MultiLineString":
            for line in ops.linemerge(geometry.MultiLineString(lines)):
                fc["features"].append(Feature(geometry=line, properties={"name": name[1], "hour": name[0]}))
        else:
            fc["features"].append(f)

    return fc
"""


"""
def absolute_numbers(routes, weight=1):
    d = {0: [], 1: [], 2: [], 3: [], 'count': [], 'path': []}
    df = pd.DataFrame(data=d)

    fc = FeatureCollection(features=[])
    for idx, route in enumerate(routes["features"]):
        if 'demand' in route["properties"].keys():
            for hour, cnt in route["properties"]["demand"].items():
                #df1 = pd.DataFrame.from_dict(xi['geometry']['coordinates'])
                coordinates = route['geometry']['coordinates']
                c = [[x[0], x[1], y[0], y[1]] for x, y in zip(coordinates, coordinates[1:])]
                df1 = pd.DataFrame.from_dict(c)
                df1['hour'] = hour
                df1['count'] = int(cnt)
                df1['path'] = idx
                df = df.append(df1, ignore_index=False)

    # In[Aggregate]:
    agg = df.groupby([0, 1, 2, 3, "hour"]).agg({0: 'first', 1: 'first', 2: 'first', 3: 'first', 'count': sum, 'path': 'first', 'hour': 'first'})

    #print(agg.to_string())

    grouped = agg.groupby(['hour', 'count'])

    for name, group in grouped:
        print(name)
        lines = [geometry.LineString([[e[0], e[1]], [e[2], e[3]]]) for e in group.values.tolist()]
        f = Feature(geometry=ops.linemerge(geometry.MultiLineString(lines)), properties={"name": name[1], "hour": name[0]})
        if f["geometry"]["type"] == "MultiLineString":
            for line in ops.linemerge(geometry.MultiLineString(lines)):
                fc["features"].append(Feature(geometry=line, properties={"name": name[1], "hour": name[0]}))
        else:
            fc["features"].append(f)

    return fc
"""

@memory.cache
def make_iso_polys(G, start, trip_time, edge_buff=.0001, node_buff=.0002, infill=True):
    start_node = ox.get_nearest_node(G, (start.lat, start.lng))
    subgraph = nx.ego_graph(G, start_node, radius=trip_time, distance=speed_na)

    node_points = [geometry.Point((data['x'], data['y'])) for node, data in subgraph.nodes(data=True)]
    nodes_gdf = ox.gpd.GeoDataFrame({'id': subgraph.nodes()}, geometry=node_points)
    nodes_gdf = nodes_gdf.set_index('id')

    edge_lines = []
    for n_fr, n_to in subgraph.edges():
        f = nodes_gdf.loc[n_fr].geometry
        t = nodes_gdf.loc[n_to].geometry
        edge_lines.append(geometry.LineString([f, t]))

    n = nodes_gdf.buffer(node_buff).geometry
    e = ox.gpd.GeoSeries(edge_lines).buffer(edge_buff).geometry
    all_gs = list(n) + list(e)

    new_iso = ox.gpd.GeoSeries(all_gs).unary_union

    # try to fill in surrounded areas so shapes will appear solid and blocks without white space inside them
    if infill:
        new_iso = geometry.Polygon(new_iso.exterior)

    return Feature(geometry=json.loads(json.dumps(geometry.mapping(new_iso))),
                   properties={"name": start.name + " " + str(trip_time)})


def graph_to_geojson(G):
    feat = []
    for e in G.edges(data=True):
        if "osmid" in e[2]:  # if road
            feat.append(
                Feature(
                    geometry=LineString([(G.node[e[0]]['x'], G.node[e[0]]['y']), (G.node[e[1]]['x'], G.node[e[1]]['y'])]),
                    properties={"street": e[2].get("name", "undefined"), "s":e[0], "e": e[1]}
                )
            )

    return FeatureCollection(features=feat)


def toggle_knip(G, start, end):
    try:
        G.edges[start, end, 0]["knip"] = not G.edges[start, end, 0].get("knip", False)
    except:
        pass

def calculate_time_google(start, end, transport="driving"):
    gmaps = googlemaps.Client(key=google_key)

    directions_result = gmaps.directions((start.lat, start.lon),(end.lat, end.lon), mode=transport)
    #TODO sum of al legs, assuming 1
    return directions_result[0]["legs"][0]["duration"]["value"]


def distance_matrix_google(src, dest):
    gmaps = googlemaps.Client(key=google_key)

    destinations = [(d.lat, d.lng) for d in dest]

    time_matrix = pd.DataFrame(index=src, columns=dest)
    for s in src:
        gmatrix = gmaps.distance_matrix(origins=[(s.lat, s.lng)], destinations=destinations)
        #pprint.pprint(gmatrix)

        # Only 1 row (1 source, to bypass Timeouts)
        row = [desti["distance"]["value"] for desti in gmatrix["rows"][0]["elements"]]
        time_matrix.loc[s] = row

    return time_matrix