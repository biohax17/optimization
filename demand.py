import json
import pickle

import osmnx as ox, networkx as nx, geopandas as gpd, matplotlib.pyplot as plt
from geojson import FeatureCollection, Point, Feature, LineString

from generate_demand.generate_locations import *
from model import *
import geojson


def simulate():
    ox.config(log_console=True)

    center = Location(lat=51.057002, lng=3.728418)  # Charla
    # Creates list of roads from json
    #with open('accessroads.json', 'r') as roadsfile:
    #    json_roads = json.load(roadsfile)

    #pprint.pprint(json_roads)

    src = monte_carlo(center, 8, 8, 40)  # random punten genereren

    # download the street network
    G = ox.graph_from_file("data/vlaanderen.osm", simplify=True)
    with open('vlaanderen.pkl', 'wb') as output:
        pickle.dump(G, output, pickle.HIGHEST_PROTOCOL)
    #G = ox.load_graphml("vlaanderen.graphml")
    ox.save_gdf_shapefile(G,"kaka")
    print("gedaan")
    #ox.save_graphml(G,"vlaanderen.graphml")
    #G = ox.load_graphml("vlaanderen.graphml")


    # find the centermost node
    center_node = ox.get_nearest_node(G, (center.lat, center.lng))

    # Preprocessing
    for ar in json_roads:
        ar["demand"] = 0    # Reset
        ar["node"] = ox.get_nearest_node(G, (ar["lat"], ar["lng"]))

    routes = []

    for s in src:
        start_node = ox.get_nearest_node(G, (s[1], s[0]))
        try:
            route = nx.shortest_path(G, start_node, center_node, weight=routingutils.speed1)
            routes.append(LineString(geometry=[Point((G.nodes[node]["x"], G.nodes[node]["y"])) for node in route]))
            for ar in json_roads:
                if ar["node"] in route:
                    ar["demand"] += 1

        except nx.NetworkXNoPath:
            print(s)

    df = FeatureCollection([Feature(geometry=Point((G.nodes[n["node"]]["x"], G.nodes[n["node"]]["y"])),
                                    properties={"name": n["name"], "demand": n["demand"]}) for n in json_roads])

    sf = FeatureCollection([Feature(geometry=Point((s[1], s[0]))) for s in src])

    routes = FeatureCollection(routes)

    return df, sf, routes

if __name__ == "__main__":
    simulate()