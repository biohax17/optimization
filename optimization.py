import timeit
from enum import Enum

import pulp
import json
from model import *
import pandas as pd


class Objective(Enum):
    MIN_COST = 1
    MIN_TRAFFIC = 2
    MAX_FLOW = 3

def optimize(center, access_roads, parking_candidates, shuttles, G, demand,
             objective=Objective.MIN_TRAFFIC.value, natrans=1800, margin=1800, budget=8500, max_shuttles=2, k=.178):

    start_time = timeit.default_timer()
    T = routingutils.distance_matrix_osm(G, access_roads, [center])
    elapsed = timeit.default_timer() - start_time
    print("Calculated paths to center(", len(access_roads),") in",elapsed)

    start_time = timeit.default_timer()
    #print(parking_candidates.__newobj__)
    t = routingutils.distance_matrix_osm(G, access_roads, parking_candidates.__wrapped__)
    elapsed = timeit.default_timer() - start_time
    print("Calculated paths from parkings(", len(access_roads)*len(parking_candidates), ") in", elapsed)

    objective = Objective(objective)
    print(objective)

    # Create acceptance matrix (also with shuttle)
    #a = pd.DataFrame(index=access_roads, columns=parking_candidates)
    #a_shuttle = pd.DataFrame(0, index=access_roads, columns=parking_candidates)

    print("determine feasability")
    start_time = timeit.default_timer()
    a, a_shuttle = routingutils.natras_feas_matrix(G, access_roads, list(parking_candidates), dict(shuttles), center, natrans)
    """
    # TODO threading
    # Na transport constraint only
    for p in parking_candidates:
        feas = int(routingutils.route_osm_duration(G, p, center, routingutils.speed_na) <= natrans)
        a[p] = feas
        if not feas and (p.shuttle != "0"):  # Try shuttle if available
            shut_feas = 1 if shuttles[p.shuttle].duration <= natrans else 0
            a_shuttle[p] = shut_feas
    """

    if objective==Objective.MAX_FLOW:
        # Create acceptance matrix for flow interception
        along_path = routingutils.along_path_matrix(G, access_roads, parking_candidates, center, margin)
        along_path = along_path.reindex_like(a)
        print(along_path.to_string())

        a = pd.DataFrame(a.values * along_path.values, columns=a.columns, index=a.index)    # cell wise multiplication
        a_shuttle = pd.DataFrame(a_shuttle.values * along_path.values, columns=a_shuttle.columns, index=a_shuttle.index)    # cell wise multiplication

        print(a.to_string())
        print(a_shuttle.to_string())
    elapsed = timeit.default_timer() - start_time
    print("Determine feas(", len(access_roads) * len(parking_candidates), ") in", elapsed)

    print(a.to_string())
    print(a_shuttle.to_string())


    # Instantiate our problem class
    model = pulp.LpProblem("Parking assignment", pulp.LpMinimize)

    # Decision variables
    X = pulp.LpVariable.dicts("Parking", parking_candidates, lowBound=0, upBound=1, cat=pulp.LpInteger)
    Y = pulp.LpVariable.dicts("Flow", (parking_candidates, access_roads), lowBound=0, upBound=1)
    Z = pulp.LpVariable.dicts("Shuttle", parking_candidates, lowBound=0, upBound=1, cat=pulp.LpInteger)


    # Objective function
    if objective==Objective.MAX_FLOW:
        model += pulp.lpSum([[(1-Y[j][i])*demand[i].sum() for i in access_roads] for j in parking_candidates]),\
                 "Max number of cars out of city/ max flow interception"


    elif objective==Objective.MIN_TRAFFIC:
        model += pulp.lpSum([[Y[j][i]*2*(t.loc[i][j] - T.loc[i][0])*demand[i].sum() for i in access_roads] for j in parking_candidates]),\
                "Maximize saved km's"

    elif objective==Objective.MIN_COST:
        model += pulp.lpSum(\
                [X[j] * j.price + Z[j] * shuttles[j.shuttle].cost \
                - [k/1000 * Y[j][i]*2*(T.loc[i][0] - t.loc[i][j])*demand[i].sum() for i in access_roads] for j in parking_candidates]),\
                "Minimize social cost"


    # Constraints
    #Respect feasability
    for road in access_roads:
        for p in parking_candidates:
            model += Y[p][road] <= a.loc[road][p] * X[p] + a_shuttle.loc[road][p] * Z[p]

    # Capacity of parking
    for parking in parking_candidates:
        model += pulp.lpSum(Y[parking][road] * demand[road].sum() for road in access_roads) <= \
                 X[parking] * parking.capacity, \
                 "Respect cap %s " % parking

    # Max number of shuttles
    model += pulp.lpSum([Z[parking] for parking in parking_candidates]) <= max_shuttles, "upperbound number of shuttles"

    # Only use shuttle if parking is used
    for j in parking_candidates:
        model += X[j] >= Z[j], "Only use shuttle if %s parking used" % j

    # Budget constraint
    model += pulp.lpSum(X[j] * j.price + Z[j] * shuttles[j.shuttle].cost for j in parking_candidates) \
             <= budget, \
             "Respect budget"

    # Integrity
    for road in access_roads:
        model += pulp.lpSum(Y[parking][road] for parking in parking_candidates) <= 1, "%s"%road

    # Save to disk
    model.writeLP("P+RProblem.lp")

    # Solve our problem
    print("start solving...")
    start_time = timeit.default_timer()
    model.solve()
    elapsed = timeit.default_timer() - start_time
    print("Solved MILP in", elapsed)

    print(pulp.LpStatus[model.status])
    print(pulp.value(model.objective))

    # Print info
    print("Totaal: €", spend_budget_parking(X, parking_candidates)+spend_budget_shuttle(Z, shuttles, parking_candidates))
    print("Parking: €", spend_budget_parking(X, parking_candidates))
    print("Shuttles: €", spend_budget_shuttle(Z, shuttles, parking_candidates))
    print("Saved km's: ", saved_kms(access_roads,parking_candidates, demand, T, t ,Y))
    print("Captured cars: ", captured_cars(access_roads, parking_candidates, demand, Y))
    print("Social cost: ", social_cost(access_roads, parking_candidates,shuttles,demand, T,t,X,Y,Z,k))
    print_shuttles(Z, parking_candidates)
    update_parkings(X, Y, parking_candidates, access_roads, demand)

    flows, rest = create_flows(Y, access_roads, parking_candidates)
    return flows, rest


def update_parkings(X, Y, parking_candidates, access_roads, demand):
    # Print gebruikte parkings
    for parking in parking_candidates:
        if X[parking].varValue==1: #Parking is used
            parking.used = True
            parking.capacity = parking.initial_capacity\
                               - int(sum([Y[parking][r].varValue * demand[r].sum()  for r in access_roads]))
            print(parking, ": ", parking.initial_capacity - parking.capacity, "/", parking.initial_capacity)


def print_shuttles(Z, parking_candidates):
    # Print gebruikte parkings
    for parking in parking_candidates:
        if Z[parking].varValue==1: #Parking is used
            print(Z[parking])


def create_flows(Y, access_roads, parking_candidates, save=False):
    #Print flows
    flows = []
    rest = []
    for a_id in range(len(access_roads)):
        intercepted = 0
        for p_id in range(len(parking_candidates)):
            intercepted += Y[parking_candidates[p_id]][access_roads[a_id]].varValue
            flows.append({"parking": p_id,
                          "accessroad": a_id,
                          "flow": Y[parking_candidates[p_id]][access_roads[a_id]].varValue})
        rest.append({"accessroad": a_id, "flow": 1-intercepted})

    if save:
        with open('flows.json', 'w') as outfile:
            json.dump(flows, outfile)

    return flows, rest


def saved_kms(access_roads, parkings, demand, T, t, Y):
    return sum([Y[j][i].varValue*2*(T.loc[i][0] - t.loc[i][j])*demand[i].sum() for i in access_roads for j in parkings]) / 1000


def captured_cars(access_roads, parkings, demand, Y):
    return int(sum([Y[j][i].varValue*demand[i].sum()  for i in access_roads for j in parkings]))


def social_cost(access_roads, parkings, shuttles, demand, T, t, X, Y, Z, k):
    return spend_budget_parking(X, parkings)\
           + spend_budget_shuttle(Z, shuttles, parkings)\
           - k * saved_kms(access_roads, parkings, demand, T, t, Y)


def spend_budget_parking(X, parkings):
    return sum([X[j].varValue * j.price for j in parkings])


def spend_budget_shuttle(Z, shuttles, parkings):
    return sum([Z[j].varValue * shuttles.get(j.shuttle).cost for j in parkings])
