import pickle
import sys

import pandas as pd
from PyQt5.QtWidgets import QApplication


from gtfs2graph import get_stops, gent_as_feed
from gui.MapView import MapView
from model import *
from util.decorators import async

import networkx as nx

# Center partyzone
center = Location(lat=51.057002, lng=3.728418)  # Charla


# Creates list of parkings from json
with open('data/parkings.json', 'r') as parkingfile:
    json_parkings = json.load(parkingfile)

# Convert to Parking objects
parking_candidates = []
for jp in json_parkings:
    parking_candidates.append(Parking(**jp))

# Creates list of roads from json
with open('data/accessroads.json', 'r') as roadsfile:
    json_roads = json.load(roadsfile)

# Convert to Road objects
access_roads = []
for jr in json_roads:
    access_roads.append(AccessRoad(**jr))

# Creates dict of shuttles from json
with open('data/shuttles.json', 'r') as roadsfile:
    json_shuttles = json.load(roadsfile)

# Convert to Shuttle objects
shuttles = {}
for id, shuttle in json_shuttles.items():
    shuttles[id] = Shuttle(**shuttle)


feed = gent_as_feed()


"""
G_gtfs = create_average_graph(feed)
G_osm = routingutils.gent_as_graph()
G = routingutils.concat_graphs(G_osm, G_gtfs)

#for node, data in G_osm.nodes(data=True):
#    print(type(node), type(data['x']), type(data['y']))


#ox.save_graphml(G, "gtfs.graph")




#ox.save_graphml(G, filename='gent_gtfs.graphml')
with open('graph.pkl', 'wb') as output:
    pickle.dump(G, output, pickle.HIGHEST_PROTOCOL)

#G = ox.load_graphml("gtfs.graph")
"""




#G = ox.load_graphml(filename='gent_gtfs.graphml')
#for node, data in G.nodes(data=True):
#    print(data)


with open("graph.pkl", "rb") as input_file:
    G_old = pickle.load(input_file)
    G = HashableGraph(G_old)
#"""

demand = pd.read_csv("data/150717.csv", parse_dates=True, index_col=0)
demand.columns = demand.columns.map(int) # cast to ints
demand.columns = demand.columns.map(access_roads.__getitem__) # index to object
#TODO eenmalig
demand=demand.applymap(lambda x: x*.45)

if __name__ == "__main__":
    sys.argv.append("--disable-web-security")
    app = QApplication(sys.argv)

    mp = MapView(G, feed, center, access_roads, parking_candidates, shuttles, demand)
    # TODO wait for better scaling support
    mp.setZoomFactor(2.0 if app.desktop().logicalDpiX() > 96 else 1.0)
    mp.show()
    sys.exit(app.exec_())  # only need one app, one running event loop