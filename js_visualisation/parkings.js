parkings = [
    {"name": "P+R The Loop/Expo", "lat": 51.0247, "lng": 3.69498, "capacity": 168, "price":0},
    {"name": "P+R Gentbrugge", "lat": 51.0356, "lng": 3.75742, "capacity": 150, "price":0},
    {"name": "P+R Gentbrugge Arsenaal", "lat": 51.032564, "lng": 3.758556, "capacity": 250, "price":0},
    {"name": "P+R Moscou", "lat": 51.0294, "lng": 3.75078 , "capacity": 15, "price":0},
    {"name": "P+R Mariakerke Post", "lat": 51.0766, "lng": 3.67811, "capacity": 25, "price":0},
    {"name": "P+R Industrieweg Wondelgem", "lat": 51.0985, "lng": 3.71139, "capacity": 46, "price":0},
    {"name": "P+R Oostakker", "lat": 51.0739, "lng": 3.777, "capacity": 200, "price":0},
    {"name": "P+R Muide (Pauwstraat) ", "lat": 51.073622, "lng": 3.733547, "capacity": 50, "price":0},
    {"name": "P+R Maaltepark ", "lat": 51.023068, "lng": 3.703065, "capacity": 40, "price":0},
    {"name": "P+R Hekers ", "lat": 51.003020, "lng": 3.709605, "capacity": 68, "price":0},
    {"name": "P+R Watersportbaan (Zuiderlaan)", "lat": 51.049577, "lng": 3.687238, "capacity": 230 , "price":0},
    {"name": "P+R Bourgoyen ", "lat": 51.067211, "lng": 3.682767, "capacity": 250 , "price":0},
    {"name": "P+R Neptunus Wondelgem", "lat": 51.086945, "lng": 3.704949, "capacity": 148 , "price":0},
    {"name": "P+R Weba/Decathlon ", "lat": 51.073393, "lng": 3.742440, "capacity": 200, "price":0},
    {"name": "P+R Galveston", "lat": 51.074036, "lng": 3.722127, "capacity": 250, "price":2},
    {"name": "P+R Sint-Pietersstation", "lat": 51.036343, "lng": 3.704689, "capacity": 300, "price":0}
]
