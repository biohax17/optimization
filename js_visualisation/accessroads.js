var accessroads = [
    {"name": "R4 Oostakker", "lat": 51.096794, "lng": 3.756576, "demand": 100},
    {"name": "N70 Lochistri", "lat": 51.088119, "lng": 3.807230, "demand": 160},
    {"name": "N445 Destelbergen", "lat": 51.059679, "lng": 3.823551, "demand": 300},
    {"name": "E17 Antwerpen", "lat": 51.055347, "lng": 3.844285, "demand": 800},
    {"name": "N9 Melle", "lat": 51.005588, "lng": 3.796909, "demand": 300},
    {"name": "B403 Merelbeke", "lat": 51.000274, "lng": 3.770682, "demand": 200},
    {"name": "E40 Brussel", "lat": 51.006459, "lng": 3.742434, "demand": 700},
    {"name": "E17 Kortrijk", "lat": 50.993530, "lng": 3.712246, "demand": 890},
    {"name": "N60 Zwijnaarde", "lat": 51.004932, "lng": 3.696212, "demand": 400},
    {"name": "N43 Sint Denijs", "lat": 51.017972, "lng": 3.670710, "demand": 80},
    {"name": "E40 Oostende", "lat": 51.030910, "lng": 3.649914, "demand": 1000},
    {"name": "N466 Drongen", "lat": 51.053872, "lng": 3.665248, "demand": 150},
    {"name": "N9 Belzele", "lat": 51.086786, "lng": 3.661072, "demand": 150},
    {"name": "N456 Evergem", "lat": 51.105758, "lng": 3.708860, "demand": 80},
    {"name": "R4 Evergem", "lat": 51.110002, "lng": 3.725841, "demand": 400},
    {"name": "N458 Wondelgem", "lat": 51.103369, "lng": 3.728686, "demand": 300}
]
